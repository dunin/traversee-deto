const http = require('http');
const url = require('url');
const Story = require('inkjs').Story;
const fs = require('fs');

const hostname = '127.0.0.1';
const port = 80;

const server = http.createServer((req, res) => {
  histoire = new Story(fs.readFileSync('./histoire.ink.json', 'UTF-8').replace(/^\uFEFF/, ''));
  
  if (fs.existsSync("save.json"))  {
    histoire.state.LoadJson(JSON.parse(fs.readFileSync('./save.json', 'UTF-8')));
  }

  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');

  var urlParse = url.parse(req.url,true);
  var rep="";
  switch (urlParse.pathname.replace("/","")) {
    case "goto":
      rep = gotoSection(urlParse.query.type);
      break;
    case "choices":
      rep = getChoices();
      break;
    case "choice":
      rep = makeChoice(urlParse.query.num);
      break;
    default:
      rep = "error parsing url "+urlParse.pathname.replace("/","");
  }

  fs.writeFileSync('./save.json',JSON.stringify(histoire.state.ToJson()));

  res.end(rep||"");

  //url.parse(req.url).query -> {keysearc:val}
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});


var histoire;


function getText() {
  var rep= [];
  while (histoire.canContinue){
      rep.push(histoire.Continue().replace(/\n/,""));
  }
  return rep.join("///");
}

function getChoices() {
  var rep = [];
  for (var i = 0; i < histoire.currentChoices.length; ++i) {
      var choice = histoire.currentChoices[i];
      rep.push(choice.text);
  }
  return rep.join("///");
}

function makeChoice(id) {
  histoire.ChooseChoiceIndex(parseInt(id));
  return getText();
}


function gotoSection(nom) {
  console.log("gotoSection",nom)
  if(nom == "start") histoire = new Story(fs.readFileSync('./histoire.ink.json', 'UTF-8').replace(/^\uFEFF/, ''));
  else histoire.ChoosePathString(nom);
  return getText();
}
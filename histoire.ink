VAR f = false
VAR c = "Collecteur"
VAR temps = "trente minutes"
VAR nb = 4

INCLUDE plantes.ink
INCLUDE explosion.ink

-> intro

=== intro

Vous m’entendez ?
+   [Non.]
+   [Oui.]
    Vous m’entendez bien ?
    + +     [5 sur 5.]
            Parfait.
            Vous êtes sur place ?
            + + +   [Je suis arrivée.]
                    ~ f = true
                    ~ c = "Collectrice"
            + + +   [Je suis arrivé.]
            - - -   -> instructions
    + +     [Pas vraiment.]
-   Mince, mon appareil doit avoir un problème. Attendez, j’essaie quelquechose.
    -> intro


=== instructions

Très bien {c}, votre mission commence maintenant.
Avant tout, rappelez-vous que vous peu de temps sur place.
Dans {temps} environ, votre module spatial décollera à nouveau, et la planète 3D6E 1T5O sera abandonnée à son sort.
Il décollera avec ou sans vous, donc soyez de retour d’ici {temps} si vous ne voulez pas pourrir ici.
(et pourrir est une façon de parler ; vu la gravité de la situation, vous serez désintégré{f:e} bien avant votre putréfaction)
+   [Compris.]
+   [Euh, la gravité de la situation ?]
    Oui, on ne vous a peut-être pas entièrement prévenu{f:e}, mais il nous fallait bien des volontaires, vous comprenez n’est-ce pas ?
    + +     [Non.]
            Qu’importe. <>
    + +     [Oui.]
-   Venons-en à votre mission : cette planète sauvage regorge d’espèces de plantes que nous ne possédons pas.
    (ce qui, au vu du peu de plantes qu’il nous reste, n’est pas bien difficile)
    Cependant cette planète est menacée et toutes ces espèces risquent de disparaître dans l’oubli !
    Le consortium a donc selectionné {nb} plantes que vous devez récolter, et qui iront rejoindre les collections du jardin botanique.
    Votre détecteur vous aidera à trouver où pousse chacune de ces plantes, et à savoir laquelle recueillir.
    Des Questions ?
    + +     [Non.]
            Parfait. <>
    + +     [Quelques-unes…]
            Gardez-les pour plus tard. <>
    - -     Trouvez-moi ces plantes, et revenez avant {temps}. Le consortium compte sur vous !

    -> DONE


=== impertinence

{ impertinence:
- 1:    Et {c}, sachez que l’impertinence de votre remarque ne m’échappe pas.
        Ne m’obligez pas à faire un signalement au consortium…  
- 2:    Aussi, j’ai à nouveau déceller une pointe d’ironie dans votre ton {c}.
        Je vous adresse un dernier avertissement…
- 3:    Et {c}, je vous avais demandé de cesser vos remarques impertinentes, et vous ne m’avez pas écouté.
        Votre comportement va faire l’objet d’un signalement au Consortium.
- else: Et je vais ajouter cette remarque déplacée à la note que je prépare pour le Consortium,
        je crains qu’ils ne l’apprécient guère…
}


=== fin

Bravo {c}, vous avez récolté toutes les plantes dont nous avions besoin !
Votre Collecto 2000 va les stocker à l’intérieur du vaisseau<>
+ {impertinence>2}  ->
    .
    Vous, par contre, étant donné votre comportement, vous ne pourrez pas monter à bord.
    + + [Pardon?!]
    + + [Comment?!]
    - - Vous m’avez très bien compris {c}. Et vous ne pouvez pas dire que je ne vous avais pas prévenu{f:e}.
        Le Consortium ne peut pas se permettre de conserver des individus de votre espèce au sein de la population.
        + + +   (preservation) [Je croyais qu’il oeuvrait pour la préservation des espèces ?]
        + + +   (fantaisie) [Le Consortium devrait s’autoriser un peu plus de fantaisie, vous savez…]
        - - -   Vous avez toujours une petite remarque ironique sous le coude, {c}, et c’est bien ce qui vous a perdu{f:e}.
                Soyons franc, le Consortium est une entité politique, et son premier objectif est de perdurer dans le temps.
        + + +   {preservation} ->
                Sa sollicitude envers les espèces vivantes est proportionnelle à l’intérêt de ses administrés à leur sujet.
                Ces {nb} plantes que vous avez ramenées vont en contenter un certain nombre,
                et les autres, ceux qui, comme vous, savent que nous n’en faisons pas assez,
                ne seront plus suffisament nombreux pour être un risque pour le Consortium.
                Cependant, vous faites aussi partie de ces individus qui pourraient en convaincre d’autres, et ainsi changer les proportions.
        + + +   {fantaisie} ->
                C’est pourquoi il doit se prémunir contre les critiques trop convaincantes
                (tout en donnant l’illusion de tolérer d’autres critiques, moins efficaces).
                Certains individus, comme vous, pourraient en convaincre d’autres, et les amener à demander un autre mode de gouvernance.
        - - -   Empêcher ces individus de parler est une petite commodité, qui nous évite d’avoir à recalculer nos stratégies.
                Et c’est une petite commodité que, parfois, nous nous autorisons.
                Adieu {c}.
                -> DONE
+   {impertinence<3} ->
    , dans un module qui se détachera pour rejoindre le jardin botanique,
    tandis que vous partirez sur une autre planète, pour une autre collecte.
    + + [Pardon ?!]
    + + [Comment ?!]
    - - C’est votre rôle, et vous avez montré que vous saviez l’accomplir avec efficacité.
        Vous êtes désormais {f:une|un} {c} à part entière, prêt à vouer sa vie à la survie des espèces.
        + + +   [Il n’y a pas d’autre mission de disponible ?]
        + + +   [Vous savez, je ne me sens pas tellement {c} à part entière…]
        - - -   C’est votre destin.
                Du moins celui qu’a choisi pour vous le Consortium.
                + + + + [J’aurais aimé un destin moins dangereux…]
                + + + + [J’aurais aimé pouvoir choisir moi-même…]
                - - - - Et moi j’aurais voulu être musicien. Et pourtant je suis là, à vous parler, et pas sur scène.
                        On ne choisit pas toujours {c}.
                        Essayez de vous détendre avant votre prochaine mission.
                        Buvez un coup, écoutez une musique qui vous fait plaisir.
                        Vivez.
                        -> DONE

=== timeout

Trop tard {c}, les {temps} sont passées !
Vous avez lamentablement échoué{impertinence>1:, en nous faisant perdre du temps avec vos considérations idiotes}.
Nous ne pouvons pas prendre le risque de perdre notre module spatial,
il va donc décoller sans vous, et sans les plantes que vous {plantes>2:avez collectées|deviez collecter}.
+   [Je ne suis pas plus précieu{f:se|x} qu’un module spatial?]
    Les humains se remplacent plus facilement que les machines {c},
    et les machines nous posent moins de problèmes.
+   [Vous semblez plus triste pour les plantes que pour moi…]
    Personne ne s’intéresse à votre survie, {c},
    celle de ces plantes, par contre, est une question importante pour la société civile.
    Et le Consortium se doit de contenter la société civile.
-   Adieu {c}.
    Essayez de profiter du temps qu’il vous reste à vivre.
    -> DONE
class Histoire {
  String[] texts;
  String[] choices;
  int tId;
  public boolean debug;

  Histoire() {
    texts = null;
    choices = null;
    tId = 0;
    debug = false;
  }

  private void gotoAndSet(String section, boolean choice) {
    
    String[] textToParse, choicesToParse;
    if(choice) {
      if (this.debug) println("Histoire : ---> do choice "+section);
      textToParse = loadStrings("http://127.0.0.1:80/choice?num="+section);
    } else {
      if (this.debug) println("Histoire : ----------> goto section "+section);
      textToParse = loadStrings("http://127.0.0.1:80/goto?type="+section);
    }
    tId = 0;
    choicesToParse = loadStrings("http://127.0.0.1:80/choices");
    texts = (textToParse != null && textToParse.length>0)? split(textToParse[0], "///") : null;
    choices = (choicesToParse !=null && choicesToParse.length>0)? split(choicesToParse[0], "///") : null;
  }
  
  void gotoSection(String section) {
      gotoAndSet(section, false);
  }
  
  public boolean hasTxt() {
    return texts!=null && tId<texts.length;
  }

  public boolean hasChoices() {
    return choices!=null && (texts == null || tId==texts.length);
  }

  public String nextTxt() {
    if (this.hasTxt()) {
      tId++;
      if (this.debug) println("Histoire : print texte "+tId+" / "+texts.length);
      return texts[tId-1];
    }
    return null;
  }

  public String[] getChoices() {
    if (this.hasChoices()) {
      return choices;
    }
    return null;
  }
  
  public void doChoice(int id) {
    this.gotoAndSet(id+"",true);
  }
}

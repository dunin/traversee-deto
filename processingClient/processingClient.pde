Histoire histoire;

void setup() {
  size(500, 500);
  fill(200);
  
  
  histoire = new Histoire(); // on crée une instance d'Histoire
  histoire.debug = true; // si on veut trois infos dans la console
  histoire.gotoSection("start"); // on va a une section donnée

  /* SECTIONS DISPONIBLES :
    start -> démarrage d'une partie, réinitialisation des valeurs
    plantes -> on trouve une plante
    explosion -> y a eu un boom
    fin -> on a trouvé toutes les plantes, et on est revenu au point de départ
    timeout -> plus de temps, c'est la fin

  */
  
  setNext(0); // on simule l'appui sur un bouton, pour lancer le premier texte
}

void draw() {
}


void keyPressed() {
  // on simule les deux boutons avec les flèches du clavier
  if(keyCode == LEFT) setNext(0);
  else if (keyCode == RIGHT) setNext(1);
}

void setNext(int btn) {
  // On a appuyé sur un des deux boutons, il faut agir
  
  background(50); // on efface l'écran
  
  if (!histoire.hasTxt() && histoire.hasChoices()) { 
    // il n'y a plus de texte à afficher, mais il y a un choix à faire
    
    histoire.doChoice(btn); // btn donne l'id du choix (0 ou 1);
    
    // on ne sort pas de la fonction, parce que maintenant il y a peut-être du texte à afficher
  }
  
  if (histoire.hasTxt()) {
      // il y a du texte à afficher, donc, nous le faisons.
      
      text(histoire.nextTxt(), 10, 15, 480, 100);
      
      if(!histoire.hasTxt() && histoire.hasChoices()) {
         // Nous sommes arrivés au dernier texte à afficher
         // Et on a trouvé des choix possibles
         
        String[] choix = histoire.getChoices(); // on récupère les choix
        
        text(choix[0], 10, 50,230,100); // on les affiche
        text(choix[1],260, 50,230,100); // (on sait, dans notre cas qu'il y en aura toujours deux)
      }
    }
    
    else {
      histoire.gotoSection("plantes"); // on fait comme si on trouvait de suite une plante
      if (!histoire.hasTxt()) {
        histoire.gotoSection("fin");
      }
    }
    //else println("END"); // comme on est frustré qu'il n'y ait ni texte à afficher, ni choix à faire; alors, on affiche un texte sur la console...
}

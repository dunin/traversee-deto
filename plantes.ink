=== plantes

{ plantes:
- 1:    -> plante_1
- 2:    -> plante_2
- 3:    -> plante_3
- 4:	-> plante_4
- else: -> DONE
}

== plante_1

Parfait {c}, vous avez découvert la Detonitum Primus.
Votre Collecto 2000 va en recueillir un plant pour le jardin botanique.
+	[Pourquoi celle-ci, et pas celle de gauche qui sent bien meilleur ?]
+	[Pourquoi pas celle de droite qui se gonfle et se dégonfle d’une manière si rigolote ?]
-	La domo-sphère du jardin botanique ne peut accueillir toutes les espèces de l’univers {c}.
Il est nécessaire de faire un choix.
+	[Sur quels critères ?]
	-> criteres ->
+	[Ne faudrait-il pas plutôt construire une nouvelle domo-sphère ?]
	-> moyens ->
-	Toutes vos questions nous font perdre un temps précieux. Hâtez-vous de trouver la seconde plante {c} !

-> DONE


== plante_2

Bien {c}! Vous avez découvert la Festus Secundus.
Prenez le temps de vous reposer tandis que le Collecto 2000 la récupère.
+	{!moyens} [Ces Collectos 2000 sont d’une lenteur ! Ne pourriez-vous pas en acheter d’autres ?]
	-> moyens ->
+	{!criteres} [Celle-ci ? Vous pensez vraiment qu’elle en vaut la peine ?]
	-> criteres ->
+	[Combien y a-t-il de gens comme moi ? Combien de planètes qui se désagrègent ?]
	Je ne sais pas {c}.
	Je ne suis pas payé pour compter, mais pour contrôler les collecteurs comme vous
	et pour m’assurer que les plantes parviennent au jardin botanique sans encombre…
	+ +	[« controller » ? Moi qui pensais que vous êtiez là pour m’aider…]
		Je le suis, dans un second temps.
	+ +	[Certains ne ramènent pas les plantes ?]
	- -	Nous avons malheureusement des collecteurs moins coopératifs que vous.
		Certains tentent de fuir et de s’installer sur les planètes où nous les déposons.
		+ + +	[Ont-ils tout à fait tort ?]
		+ + +	[Et quand vous les repérez, que leur arrive-t-il ?]
				Ils se désagrègent avec leur planète, et aucune plante n’est sauvée.
				+ + + +	[À quoi cela sert-il de les surveiller si vous ne faites rien ?]
						Peut-être ne faisons-nous pas tout à fait « rien »
						mais peut-être aussi qu’il vaut mieux pour vous que vous soyez moins curieu{f:se|x}.
						-> DONE
				+ + + +	[Certains disent qu’ils vivent heureux, en harmonie avec la nature, et que vous les chassez.]
		- - -	Vous auriez tort de croire les rumeurs qui disent que le Consortium détruit lui-même ces planètes pour construire ses infrastructures, et autres fadaises.
				Et encore plus tort de m’en parler.
				+ + +	[Il existe pourtant des planètes pleines de ces infrastructures et qui devaient être, avant, sauvages.]
						Pour chaque planète colonisée, il a été reproduit un écosystème
						+ + + +	{criteres.ecosysteme}	[Je croyais que l’écosystème était un concept dépassé…]
						+ + + +	{!criteres.ecosysteme}	[Peut-on être sûr que l’écosystème a été parfaitement reproduit ?]
						+ + + +							[Reproduit à l’échelle d’une planète ? Ou dans une version réduite ?]
						- - - -							Je ne suis pas tenu de répondre à vos questions, {c}.
						<- impertinence
				+ + +	[C’est vous qui parlez de ces rumeurs, pas moi.]
						Vous avez raison. Arrêtons là cette discution, et tout ira pour le mieux.

-	Bon, la troisième plante ne va pas se trouver toute seule !
	Allons, dépêchez-vous !
-> DONE


== plante_3

Bravo {c}, vous avez repéré la Triginta Sex Quindecim. Le Collecto 2000 va en prendre une bouture.
+	(presence_planete) [Pourquoi y a-t-il des bâtiments sur cette planète ?]
	Disons que lorque je vous ai dit que cette planète était sauvage, j’ai fait un raccourci.
	Il fut un temps où des hommes y vivaient. Mais ses ressources s’épuisèrent, et nous dûmes l’abandonner.
	+ +	[Visiblement, sans les hommes, la planète a su se regénérer…]
		Il est facile de supprimer les hommes de vos équations,
		seulement ils existent. Et ils ont besoin de vivre.
		+ + +	[Ils pourraient vivre différement.]
		+ + +	[Détruire est une drôle de manière de vivre.]
		- - -	Les hommes ont essayé de changer leurs habitudes, {c}.
				Avant l’exode spatial, ils voulurent conserver leur planète.
				Ils prirent des douches courtes, compostèrent leurs déchets et certains se déplacèrent même à vélo.
				Seulement, si un homme seul peut essayer de changer, les associations d’hommes n’en sont pas capables,
				et les ressources de cette planète s’épuisèrent malgré tout.
				+ + + +	[Les « associations »? Vous voulez parler des entreprises privées ?]
						Je parle de tous ces groupements qui font vivre l’Homme avec ses semblables, et agréablement.
						Ces groupements dont on ne peut prendre le risque qu’ils périclitent. Qu’on ne peut donc changer.
						+ + + + +	[Jusqu’à ce que mort de planète s’ensuive.]
									Deviendrez-vous plus cynique que moi {c} ?
						+ + + + +	[Qui ne changent pas parce que certains ne le veulent pas, plutôt.]
									C’est une remarque optimiste ou pessimiste {c} ?					
				+ + + + [Vous voulez parler du Consortium ?]
						Le Consortium est de loin ce qui s’est fait de mieux en matière de gouvernance,
						et toute critique à son égard diminue son efficacité, c’est pourquoi je ne me permettrais pas de commenter son action.
						Et vous non plus.
						<- impertinence
	+ +	(la_terre) [Euh, vous êtes en train de me dire que je suis sur la Terre{minitel:… Parce que le Minitel c’est avant l’exode spatial, on est d’accord} ?]
		Vous posez trop de questions {c}.
		+ + +	[Ne sommes-nous pas censés laisser la Terre se regénérer ?]
		+ + +	[Quelles magouilles prépare le Consortium ?]
		- - -	Vraiment {c}, vous posez BEAUCOUP trop de questions.
				+ + + + [Pourquoi ces explosions sur la Terre ? Est-ce des atterissages de collecteurs comme moi ?]
				+ + + + [Le Consortitum est-il en train de piller la Terre, malgré ses engagements ?]

+	[Elle a un drôle de nom celle-là !]
	Vous le trouvez rigolo parce qu’il y a « Sex » dedans, j’imagine.
	Sachez que Triginta Sex Quindecim est un nom latin qui veut dire trente-six quinze.
	Cette plante a été nomée ainsi parce qu’elle fût retrouvée dans un ancien appareil de communication nommé « Minitel ».
	Sur ce « Minitel », il fallait taper des nombres pour communiquer, et bien souvent ces nombres étaient trente-six et quinze.
	+ + [Merci pour ces explications, je n’en demandais pas tant.]
		Minitel voulait dire « Médium interactif par numérisation d’information téléphonique ».
		(mais comme de nos jours on ne sait plus ce qu’est un téléphone, ça ne doit pas vous évoquer grand-chose)
		Il fut utilisé entre 1980 et 2012, date de l’arrêt définitif du service.
		+ + +	[Merci, j’en sais assez.]
		+ + +	[C’est bon, j’ai compris.]
		- - -	Le principal problème du Minitel était qu’il utilisait la ligne téléphonique,
				et mon arrière grand-père me racontait souvent combien il était difficilement de téléphoner lorsque son père consultait 3615 DÉTO pour avoir le programme de son fetsival préféré…
				+ + + +	[Pitié, arrêtez-vous.]
				+ + + +	[Tout ceci ne m’intéresse pas du tout.]
				- - - -	Il faut noter que ce problème de ligne occupée existait encore lors des débuts de l’Internet
						(mais comme de nos jours on ne sait plus ce qu’est l’Internet, ça ne doit pas vous évoquer grand-chose)
						Internet ressemblait au grand réseau du Consortium, mais était beaucoup moins contrôllé. Un véritable repère d’anarchistes !
						Heureusement que le marché est venu réguler ce capharnaüm !
						+ + + + +	[STOP !]
						+ + + + +	[STOP !]
	+ +	(minitel) [Comment ce « Minitel » s’est-il retrouvé sur une planète sauvage ?]
		-> presence_planete

-	Bon sang {c}! Vous nous avez fait perdre un temps fou avec vos digressions !
	Allez vite me trouver la dernière plante !
-> DONE


== plante_4

Vous avez trouvé l’Ultimum Herba {c}!
Elle existe donc encore ! Prenez-en soin, c’est le dernier plant qui existe de cette espèce…
+	[Il ne faut pas deux plants pour qu’une plante se reproduise ?]
	Si. C’est pourquoi ce plant est si important : il est et restera le seul de son espèce !
	+ +	[Bref, l’espèce est fichue.]
		Peut-être, mais il nous la faut.
	+ +	[On ne devrait pas plutôt essayer de préserver une espèce viable ?]
		Non, il faut que vous rameniez cette espèce, et surtout celle-ci.
+	[C’est pas un peu risqué de le déplacer du coup ?]
	C’est un risque à prendre. Ce plan est très important pour nous.
-	Nos subventions en dépendent.
+	[Je vois, encore une histoire de sous…]
		L’argent est une chose nécessaire. <>
+	[En soi, vous vous moquez de la préservation des espèces…]
		Au contraire, nous nous en soucions tellement que nous essayons d’avoir assez de moyens.
-	Toute action est impossible sans moyen.
	Et pour que les gens nous donnent les moyens, il faut qu’ils se sentent concernés.
	Alors il nous faut des espèces phares, qui leur parlent, leur donnent envie d’investir dans nos plans de sauvegarde.
+	[Les gens ne peuvent pas se sentir concernés par toutes les espèces vivantes ?]
	Je ne crois pas. En tout cas, ça n’a jamais marché.
	Regardez le Jardin Zoologique : si l’on considère les espèces qui y vivent,
	on a l’impression que la Terre, avant sa destruction était peuplée de condors, de dauphins et de koalas…
	Mais c’est faux ! Elle était peuplée d’animaux que nous ne connaissons plus,
	et qui s’appelaient acariens, limaces, gardons, pigeons, rats taupiers…
	Mais il n’y a eu des fonds que pour des espèces rares et emblématiques,
	et quand les choses se sont emballées, il n’y avait plus rien pour les autres bêtes.
	+ +	[Vous êtes en train de me dire que votre système ne fonctionne pas, en somme…]
		Peut-être, mais c’est pourtant celui qui fonctionne le mieux, étant donné l’Homme.
		+ + +	(vision_Consortium) [Étant donné VOTRE vision de l’homme.]
									Ma vision est celle que partage le Consortium,
									et que se doivent de partager tous les citoyens.
									<- impertinence
		+ + +	[Ou peut-être avez-vous la flemme de chercher un autre système.]
									Vous n’êtes pas plus savant{f:e} que moi, {c}, laissons donc ces questions aux spécialistes.
									<- impertinence
	+ +	[Ce qui confirme qu’il aurait mieux valu se concentrer sur l’ensemble des êtres vivants.]
		« L’ensemble des êtres vivants » est une chose bien trop vaste et bien trop abstraite.
		Elle n’intéresse personne, et c’est une chose impossible à protéger dans son ensemble…
		+ + +	[Impossible si l’on n’est pas prêt à certains sacrifices.]
				Il faudrait que tous les hommes y soient prêts, et c’est cela qui est impossible.
				+ + + + [Vous méprisez l’Homme, et ne savez pas ce dont il est capable.]
						> vision_Consortium
				+ + + + [Ne peut-on forcer les hommes ?]
						La liberté des hommes est un des fondements du Consortium.
						Il n’est pas envisageable de revenir là-dessus.
						Même si les autres êtres vivants doivent en faire les frais.
						+ + + + +	[Joli programme.]
									N’est-ce pas ?
									<- impertinence
						+ + + + +	[Je suis donc libre de prendre une autre plante, plutôt que celle-ci ?]
									Ne soyez pas idiot{f:e}, {c}.
									Je parle abstraitement. Vous, vous devez accomplir votre mission.
									<- impertinence
		+ + +	[Elle m’intéresse, moi.]
				Et vous n’êtes personne. Ce qui confirme mes propos.
+	[Ce n’est pas plutôt au Consortium d’investir dans le plan de sauvegarde ?]
+ +	{!moyens} -> moyens
+ +	{moyens} Nous en avons déjà parlé {c}, la gestion du budget du Consortium est une chose délicate,
	et l’Homme est la priorité du Consortium.
	Même si les autres êtres vivants doivent en faire les frais.
	+ + +	[Joli programme.]
			N’est-ce pas ?
			<- impertinence
	+ + +	[Ne valons-nous pas autant que les autres êtres vivants ?]
			Vous peut-être. Mais moi et les autres membres du Consortium, certainement pas.
			Et tant qu’il existera des êtres tels que nous, les autres êtres seront, de fait, moins prioritaires.
			+ + + +	[Tant que vous serez au pouvoir.]
					Et nous le serons toujours.
					Seul le Consortium est capable de prendre les bonnes décisions pour l’humanité.
					<- impertinence
			+ + + +	[Ça va les chevilles ?]
					Il semblerait que cette planète vous ait rendu{f:e folle| fou}, {c},
					vous employez avec moi un langage bien trop familier.
					<- impertinence
-	Le temps presse {c}, retournez au module spatial, et ne me contactez plus que si c’est absolument nécessaire pour le bon déroulement de votre mission !
-> DONE


== criteres

C’est une commision du Consortium qui détermine les espèces à sauver prioritairement.
Elle se base sur trois critères : le risque pour l’espèce, le prix du plan de sauvegarde, les chances de réussite de ce plan.
+	[Et qu’advient-il des espèces qui ne sont pas prioritaires ?]
	Elles survivent par leurs propres moyens, ou elles disparaissent.
	Cela peut sembler cynique, mais c’est une simple constatation.
	+ +	[Une simple constatation cynique.]
		Ne jouez pas sur les mots.
		Nous avons un temps limité, un espace limité et un budget limité, c’est tout.
		<- impertinence
		->->
	+ +	[Et si une de ces espèces s’avérait indispensable, à la survie d’autres espèces par exemple.]
		Avec des « si », on ferait renaître Paris de ses cendres.
+	[Et comme pour tout ce qui concerne le Consortium, le choix s’effectue en toute transparence, j’imagine…]
	Que sous-entendez-vous {c}?
	+ +	[Rien. Si ce n’est qu’il est difficile de croire que vos critères ne soient pas seulement mercantiles.]
		Ce n’est pas à vous d’y croire, mais au public.
		<- impertinence
		+ + +	[Le public ne se laissera jamais berner, il se soucie de la vie.]
			Vous avez tort, les gens s’en moquent, nous ne sommes plus au début du millénaire où l’on espérait encore tout faire tenir.
		+ + +	[S’il y a de l’impertinence dans mes propos, il y a du mépris dans les vôtres.]
			À la bonne heure, {f:Madame|Monsieur} pense encore que les gens se soucient de l’écosystème ?
	+ +	[Rien, car je sais ce que je risque s’il m’arrivait de sous-entendre quoi que ce soit.]
		Vous faites bien.
		Le Consortium prend soin de nous et de tous les êtres vivants, il n’y a rien à lui reprocher.
		Si une décision du Consortium vous parait mauvaise, c’est que vous l’avez mal comprise.
		->->
-	(ecosysteme) L’écosystème est un concept dépassé, le public n’y croit plus.
	Il attend seulement de nous que nous limitions les pots cassés.
	Alors nous sauvons quelques vases remarquables, et nous les posons sur un présentoire, pour qu’il ne regarde pas les débris au sol.
	->->


== moyens
Avec quels fonds ? Le Consortium ne peut, chaque année, allouer tout son budget au seul département de préservation.
+	[Tout son budget, vraiment ?]
	C’est une façon de parler, évidemment.
	Mais n’oubliez pas que la devise du Consortium est « l’humain avant tout ».
	Donc le budget est alloué en priorité aux activités humaines.
	+ +	[Vous voulez parler de l’exploitation de son prochain ?]
	+ +	[Vous voulez parler des bibliothèques ?]
	- -	Je veux parler de n’importe quelle activité humaine.
		Ou plutôt, je ne veux parler de rien du tout. Votre opinion à ce sujet m’indifère.
		<- impertinence
+	[Je comprends, il faut savoir rester raisonnable.]
	Tout à fait {c}. Rester raisonnable et tenir sa langue.
	Vous apprenez vite. Peut-être allez-vous rentrer sain{f:e} et sau{f:ve|f} finalement…
	+ +	[Pardon ?!]
	+ +	[Quoi ?!]
	- - Oh, rien. Une blague que nous faisons parfois au poste de contrôle…
- ->->

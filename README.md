# Traversée

Dumb wrapping to use [ink form inklestudios](https://www.inklestudios.com/ink/) in a processing project.
With a simple example of a processing sketch,
and the story for the game *Traversée* for the festival **Détonation** 2019.


## Installation

```
npm install
```

## Launch

```
node index.js 

Then launch processingCLient/processingClient.pde
```

The file Histoire.pde contain the class to use in your project


## Histoire

De fait pour l'instant:

*	L'introduction (*histoire.gotoSection("start")*).
*	**2** passages après la décoouverte de plantes (*histoire.gotoSection("plantes")*, c'est le programme qui gère le fait que ce soit la première puis la seconde).

=== explosion

+	[Qu’est-ce que c’était que ça ?]
+	[Il vient d’y avoir une sorte de secousse…]
-	Cette planète se recouvre d’explosions, hâtez-vous !
	+ +	[Euh, mais c’est dangereux…]
		Très.
		Donc, comme je vous l’ai déjà dit : dépêchez-vous !
		+ + +	(rien_demande) [Attendez une minute, j’ai pas demandé à être sur une planète pleine d’explosions !]
				Peut-être, mais vous y êtes, et vous n’en partirez pas tant que vous n’avez pas récolté {nb} plantes.
				C’est bien clair ?
				+ + + +	[Ce qui est clair, c’est que je retourne au vaisseau de suite !]
						Et vous trouverez porte close.
						Donc faites votre mission, un point c’est tout.
				+ + + +	[Ce qui est clair, c’est que ma vie ne vaut pas grand-chose à vos yeux…]
		+ + +	[Mais, il s’agit de ma vie tout de même !]
		- - -	Prenez un peu de hauteur {c}. Vous êtes ici pour le bienfait du monde vivant.
				+ + + +	[Pourquoi vous n’y allez pas, vous, sur cette planète ?]
				+ + + +	[Pourquoi moi ?]
				- - - -	Chacun sa place {c}, la mienne est de diriger, la vôtre d’éxecuter.
						Nous formons une équipe et une équipe ne peut bien fonctionner que si chacun accomplit son rôle.
						+ + + + +	[Je comprends.]
									Je n’en attendais pas moins de vous.
									Maintenant, poursuivez votre mission, vite !
						+ + + + +	[Seulement vous, vous avez choisi votre rôle, pas moi.]
									C’est le propre des élites de savoir ce qui est le mieux pour tous.
									+ + + + + +	[Le mieux pour les élites, oui.]
												Il faut savoir accepter son rôle. Dites-vous qu’au moins vous, vous n’avez pas de responsabilité à assumer.
												Et cessez de vous plaindre.
												<- impertinence
												Continuez votre mission, de toute façon, vous ne pouvez faire que ça.
									+ + + + + +	[Moi, je me connais, et cette situation n’est pas la mieux pour moi…]
												Faites donc un peu preuve de bravoure {c}, et poursuivez votre mission en arrêtant de vous plaindre,
												ce sera plus agréable pour tout le monde…
												Allez, en avant !
	+ +	[Comment une planète peut se « recouvrir » d’explosions ?]
		Ceci n’est pas une question qui vous intéresse…
		+ + +	[Un peu tout de même, je vous signale que je n’ai pas demandé à être sur une planète pleine d’explosions !] -> rien_demande
		+ + +	[Ça me semble bien louche votre histoire…]
				Ne cédez pas au doute paralysant,
				faites-moi confiance et comportez-vous en {f:héroine|héros}!
	- -	\(et surtout, croisez les doigts, pour que rien ne vous tombe dessus)
-> DONE